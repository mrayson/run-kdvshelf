"""
Run the KdV equations with different amplitudes over a flat region
"""

import numpy as np 
import scipy.io as io
import matplotlib.pyplot as plt
from datetime import datetime

import xarray as xray
import yaml

from iwaves.kdv.kdvimex import  KdVImEx as KdV
#from iwaves.kdv.kdv import KdV 
from iwaves.utils.iwaveio import vkdv_from_netcdf
from iwaves.utils.viewer import viewer, animate_kdv

import pdb

def zeroic(x, a_0, L_w, x0=0.):
    return 0*x

def run_kdvimex(infile, a0, rhofile, depthfile, verbose=True):
    # Parse the yaml file
    with open(infile, 'r') as f:
        args = yaml.load(f)

    kdvargs = args['kdvargs']
    kdvargs.update({'wavefunc':zeroic})

    runtime = args['runtime']['runtime']
    ntout = args['runtime']['ntout']
    xpt =  args['runtime']['xpt']
    

    # Parse the density and depth files
    rhotxt = np.loadtxt(rhofile, delimiter=',')
    depthtxt = np.loadtxt(depthfile, delimiter=',')


    # Initialise the KdV class
    mykdv = KdV(rhotxt[:,1],\
    	rhotxt[:,0],\
        x=depthtxt[:,0],\
        **kdvargs)

    # Find the index of the output point
    idx = np.argwhere(mykdv.x > xpt)[0][0]

    # Initialise an output array
    nsteps = int(runtime//mykdv.dt_s)
    nout = int(runtime//ntout)
    B = np.zeros((nout, mykdv.Nx)) # Spatial amplitude function
    tout = np.zeros((nout,))

    B_pt = np.zeros((nsteps, )) # Spatial amplitude function
    tfast = np.zeros((nsteps,))

    output = []

    def bcfunc(t):
        omega = 2*np.pi/(12.42*3600.)
        return -a0*np.sin(omega*t)# * (1- np.exp(-t/600 ) )
        

    ## Run the model
    nn=0
    for ii in range(nsteps):
        # Log output
        point = nsteps//100 + 1
        if verbose:
            if(ii % (5 * point) == 0):
                 print( '%3.1f %% complete...'%(float(ii)/nsteps*100))
                 print(mykdv.B.max(), bcfunc(mykdv.t))

        if mykdv.solve_step(bc_left=bcfunc(mykdv.t)) != 0:
            print( 'Blowing up at step: %d'%ii)
            break
        
        ## Evalute the function
        #if myfunc is not None:
        #    output.append(myfunc(mykdv))

        # Output data
        if (mykdv.t%ntout) < mykdv.dt_s:
            #print ii,nn, mykdv.t
            B[nn,:] = mykdv.B[:]
            tout[nn] = mykdv.t
            nn+=1

        # Output single point
        B_pt[ii] = mykdv.B[idx]
        tfast[ii] = mykdv.t

    # Save to netcdf
    ds = mykdv.to_Dataset()

    # Create a dataArray from the stored data
    coords = {'x':mykdv.x, 'time':tout}
    attrs = {'long_name':'Wave amplitude',\
            'units':'m'}
    dims = ('time','x')

    Bda = xray.DataArray(B,
            dims = dims,\
            coords = coords,\
            attrs = attrs,\
        )

    coords = {'timefast':tfast}
    attrs = {'long_name':'Wave Amplitude Point',
            'units':'m',
            'x-coord':xpt}
    dims = ('timefast',)
    Bpt = xray.DataArray(B_pt,
            dims = dims,\
            coords = coords,\
            attrs = attrs,\
        )

    ds2 = xray.Dataset({'B_t':Bda,'B_pt':Bpt})
    print('Done')
    #return ds2.merge( ds, inplace=True )
    return ds.merge(ds2, inplace=True)
    #return ds.merge( xray.Dataset({'B_t':Bda,'B_pt':Bpt}), inplace=False )


######
#
#a0 = 20.
#infile = 'data/kdvin.yml'
#rhofile = 'data/kdv_density_NRA_March.csv'
#depthfile = 'data/kdv_bathy_NRA_flatshelf.csv'
#
#outfile = 'OUTPUT/test.nc'
######
#
if __name__=='__main__':

    import argparse
    parser = argparse.ArgumentParser(prog='KdV driver application')
    parser.add_argument('-f', '--infile', help='input yaml file',\
    	default='data/kdvin.yml')
    parser.add_argument('--rhofile', help='input density csv file',\
    	default='data/kdv_density_NRA_March.csv')
    parser.add_argument('--depthfile', help='input depth csv file',\
    	default='data/kdv_bathy_NRA_flatshelf.csv')
    parser.add_argument('-o','--outfile', help='output netcdf file',\
    	default='OUTPUT/test.nc')
    parser.add_argument('--a0', help='initial wave amplitude [m]',
    	type=float, default=10.)

    args = parser.parse_args()

    # Run the model
    ds = run_kdvimex(args.infile, args.a0, args.rhofile, args.depthfile)
    
    if args.outfile is not None:
        ds.to_netcdf(args.outfile)
        print( 'Saved to file: %s'%args.outfile)
    
    # Call the viewer class directly
    V = viewer(args.outfile, isvkdv=False, tstep=-2, ulim=1.0,\
            #xlim=[0.6e5,1.2e5],\
            ylim=[-.1,0.1],\
            xaxis='distance',\
            use_slider=True)

#V =# animate_kdv(outfile, movfile, isvkdv=True, tstep=-2, ulim=0.5,\
##        xlim=[0.6e5,1.2e5], ylim=[-40,40], xaxis='distance', use_slider=False)
##print 'Movie saved to %s.'%movfile
#
##V.ax1.set_xlim(-30,0)
##V.ax2.set_xlim(-30,0)
##
#
#ds.B_pt.plot()
#plt.show()
