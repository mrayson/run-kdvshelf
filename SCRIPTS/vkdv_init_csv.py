"""
Generate real conditions for a KdV simulation

Use real depth and stratification data
"""

import numpy as np 
import scipy.io as io
from scipy.interpolate import interp1d, PchipInterpolator
import matplotlib.pyplot as plt
from datetime import datetime

from soda.dataio.conversion.readcars import load_cars_temp
from soda.dataio.conversion.dem import DEM
from soda.utils.myproj import MyProj

from iwaves.utils.density import FitDensity, InterpDensity
from iwaves import IWaveModes
#from iwaves import kdv, solve_kdv 
from iwaves import kdv
from iwaves.kdv.vkdv import  vKdV

def doublesine(x, a_0, L_w, x0=0.):
    
    k = 2*np.pi/L_w
    eta =  - a_0 * np.cos(k*x + k*x0 + np.pi/2)
    eta[x>x0] = 0.
    #eta[x<x0-4*L_w/2] = 0.
    eta[x<x0-2*L_w] = 0.

    return eta

def triplesine(x, a_0, L_w, x0=0.):
    k = 2*np.pi/L_w
    eta =  - a_0 * np.cos(k*x + k*x0 + np.pi/2)
    eta[x>x0] = 0.
    #eta[x<x0-4*L_w/2] = 0.
    eta[x<x0-3*L_w] = 0.

    return eta


#######
# Inputs
basedir = '/home/suntans/Share/ScottReef/DATA'
#depthfile = '%s/BATHYMETRY/ETOPO1/ETOPO1_Bed_TimorSea.nc'%basedir
#depthfile = '%s/BATHYMETRY/OTPS_Grid_Ind2016.nc'%basedir
depthfile = '%s/BATHYMETRY/TimorSea_GA_GEBCO_Combined_DEM.nc'%basedir
#depthfile = '%s/BATHYMETRY/GEBCO_2014_TimorSea.nc'%basedir

carstempfile = '%s/OCEAN/CARS/temperature_cars2009a.nc'%basedir
carssaltfile = '%s/OCEAN/CARS/salinity_cars2009a.nc'%basedir

## Prelude transect
x0 = 122.840
y0 = -13.080

x1 = 123.519
y1 = -14.002

# Prelude point
xpt = 123.3506
ypt = -13.7641

x0_TS = x0
y0_TS = y0

### Rowley
#x0 = 119.005
#y0 = -17.796
#
#x1 = 120.005
#y1 = -18.938


## NRA Transet
#x0 = 115.829
#y0 = -19.269
#
#x1 = 116.418
#y1 = -20.057
#
#x0_TS = 115.829
#y0_TS = -19.0
#
#xpt = 116.
#ypt = -19.5


dx = 250/1e5 # topo spacing (degrees)
# KdV parameters
dz = 5.0
dxkdv = 50.

t0 = datetime(2017,3,1)
#t0 = datetime(2017,4,1)
#t0 = datetime(2017,5,1)
#t0 = datetime(2017,8,1)
density_func = 'double_tanh'
mode =0
spongedist = 1e4

outfile_h = 'data/kdv_bathy_Prelude.csv'
outfile_rho = 'data/kdv_density_Prelude_March.csv'
#######

# Generate x and y slice coordinates
lon = np.arange(x0, x1, dx)
Fx = interp1d([x0,x1],[y0,y1])
lat = Fx(lon)

# Compute the distance coordinate
P = MyProj('merc')
x,y = P(lon,lat)
dx = np.diff(x)
dy = np.diff(y)
dist = np.zeros_like(x)
dist[1:] = np.cumsum(np.abs(dx + 1j*dy))

# Find the dist location of the nearest prelude location
distpt = np.abs( (xpt-lon) + 1j*(ypt-lat))
closest_idx = np.argwhere(distpt == distpt.min())[0][0]
print('Closest x-coordinate = %lf.'%(dist[closest_idx]))

#plt.plot(lon,lat)
#plt.plot(xpt,ypt,'x')
#plt.plot(lon[closest_idx],lat[closest_idx],'ro')
#plt.show()


# Load the DEM and interpolate
print( 'Interpolating the depth data...')
D = DEM(depthfile)
z = D.interp(lon,lat)
#
#plt.figure()
#plt.plot(dist, z)
#plt.show()

print( 'Getting the CARS data...')
# Get the CARS data
h0 = z.min()
zrho = np.arange(h0,0, 10.)

T = load_cars_temp(carstempfile, np.array([x0_TS]), np.array([y0_TS]),\
     zrho, [t0] )[:,0,0]

S = load_cars_temp(carssaltfile, np.array([x0_TS]), np.array([y0_TS]),\
     zrho, [t0] )[:,0,0]

# Fit the density at the boundary

# Use the wrapper class to compute density on a constant grid
iw = IWaveModes(T, zrho, salt=S, density_class=InterpDensity, density_func=density_func)

phi, c1, he, Z = iw(h0,dz,mode)

# Update the wavelength to represent an internal tide

omega = 2*np.pi/(12.42*3600)
k = omega/iw.c1
Lw = 2*np.pi/k
print(Lw)

# Test initialising the kdv class
#mykdv0 = kdv.KdVImEx(iw.rhoZ, iw.Z, **kdvargs)
#mykdv0.print_params()

# Now create a domain for the vKdV class
#xkdv = np.arange(-xbuffer-2*Lw, dist[-1]+10*xbuffer+Lw, dxkdv) 
#xkdv = np.arange(-xbuffer-3*Lw, dist[-1]+10*xbuffer+Lw, dxkdv) 
xkdv = np.arange(-0.1*spongedist, dist[-1]+2*spongedist, dxkdv) 

Fh = interp1d(dist, -z, bounds_error=False, fill_value=(-z[0],-z[-1]))
hkdv = Fh(xkdv)

# Interp the depth
Fh = interp1d(dist, -z, bounds_error=False, fill_value=(-z[0],-z[-1]))
hkdv = Fh(xkdv)
# Smooth the data
for ii in range(10):
    hkdv[10:-9] = np.convolve(hkdv,np.ones((20,))/20.,mode='valid')

#
#A = doublesine(xkdv, a0, Lw, x0=0)
#plt.figure()
#plt.subplot(211)
#plt.plot(xkdv, A)
#
#plt.subplot(212)
#plt.plot(xkdv, -hkdv)
#plt.show()

print( 'Initializing the vKdV class...')

# This reduces dispersion near the open boundary
#xbuffer = spongedist/10.
##weight = 0.5+0.5*np.tanh((xkdv+xbuffer)/xbuffer)
#weight = 0.5+0.5*np.tanh((xkdv-spongedist)/xbuffer)
#plt.plot(xkdv, weight)
#plt.show()
#weight = 1.


#mykdv = vKdV(iw.rhoZ, iw.Z, \
#	hkdv, x=xkdv,\
#    mode=mode,\
#	a0=a0,\
#	Lw=Lw,\
#	x0=0,\
#    Nsubset=Nsubset,\
#    fweight=weight,\
#    nu_H=nu_H,\
#	Cmax=Cmax,\
#    nonlinear=nonlinear,\
#    nonhydrostatic=nonhydrostatic,\
#    wavefunc=triplesine,\
#    dt=dt,\
#    spongedist=spongedist,\
#    )
#
#ds = mykdv.to_Dataset()
#ds.to_netcdf(outfile)
#print( 'Done')

rho_ds = np.array([iw.Z, iw.rhoZ]).T
topo_ds = np.array([xkdv, hkdv]).T

np.savetxt(outfile_h, topo_ds, delimiter=',', fmt='%3.6f')
np.savetxt(outfile_rho, rho_ds, delimiter=',', fmt='%3.6f')
print('Save files {} and {}'.format(outfile_h, outfile_rho))

"""
######
# run the model
nsteps = int(runtime//mykdv.dt_s)

print nsteps, mykdv.dt_s

print 'Solving KdV equations for %d steps...'%nsteps
for ii in range(nsteps):
    point = nsteps/100
    if(ii % (5 * point) == 0):
        print '%3.1f %% complete...'%(float(ii)/nsteps*100)
    if mykdv.solve_step() != 0:
        print 'Blowing up at step: %d'%ii
        break

####


plt.figure()
plt.subplot(311)
plt.plot(mykdv.x, mykdv.B)

plt.subplot(312)
plt.plot(mykdv.x, mykdv.r10)

plt.subplot(313)
plt.plot(mykdv.x, -mykdv.h)

plt.show()
#
## Plot the initial conditions
psi = mykdv.calc_streamfunction()
u,w = mykdv.calc_velocity()
rho = mykdv.calc_density(nonlinear=True)

clims = [-0.7,0.7]

plt.figure(figsize=(12,12))
ax0 = plt.subplot(211)
plt.pcolormesh(mykdv.X, mykdv.Z, u, \
    vmin=clims[0], vmax=clims[1], cmap='RdBu')
cb=plt.colorbar(orientation='horizontal')
cb.ax.set_title('u [m/s]')
plt.contour(mykdv.X, mykdv.Z, rho, np.arange(20.,30.,0.25), \
        colors='k', linewidths=0.5)


ax1 = plt.subplot(212, sharex=ax0)
plt.plot(mykdv.x , mykdv.B, color='0.5')


########
# Plot the amplitude function
plt.figure(figsize=(12,6))
ax1 = plt.subplot(211)
plt.plot(mykdv.x , mykdv.B, color='0.5')
#plt.plot(mykdv.x - mykdv.c1*runtime, mykdv.B)
#plt.plot(mykdv2.x, mykdv2.B, 'r')
#plt.xlim(-1.1e4, 0.7e4)
plt.ylim(-50, 50)
#plt.xlim(8e4, 10e4)
#plt.xlim(4*Lw, 8*Lw)
plt.ylabel('B(m)')

ax2 = plt.subplot(212)
plt.pcolormesh(mykdv.X, mykdv.Z, psi, cmap='RdBu')
plt.colorbar()

plt.show()

"""
