"""
Obtain density profile fitting parameters using a combination of CARS climatology
and observations
"""

import numpy as np 
import scipy.io as io
from scipy.interpolate import interp1d, PchipInterpolator
import matplotlib.pyplot as plt
from datetime import datetime

from soda.dataio.conversion.readcars import load_cars_temp
from soda.dataio.conversion.dem import DEM
from soda.utils.othertime import datetime64todatetime

import pandas as pd
import xarray as xr

from iwaves.utils.density import FitDensity, InterpDensity
from iwaves import IWaveModes
#from iwaves import kdv, solve_kdv 
from iwaves import kdv
from iwaves.kdv.vkdv import  vKdV

def double_tanh_exp(z, beta):
    w = 1 + beta[6] + beta[7]
    return beta[0] - beta[1] *\
        (1/w * np.tanh( (z+beta[2])/beta[3] ) +\
        beta[6]/w * np.tanh( (z+beta[4]) / beta[5]) +\
        beta[7]/w * np.exp(z/beta[8]) )

def double_tanh_new(z, beta):
    w = 1 + beta[6]
    return beta[0] - beta[1] *\
        (1/w * np.tanh( (z+beta[2])/beta[3] ) +\
        beta[6]/w * np.tanh( (z+beta[4]) / beta[5]) )



def convert_time(tt):
    try:
        dt= datetime.strptime(tt, '%Y-%m-%dT%H:%M:%S')
    except:
        dt= datetime.strptime(tt, '%Y-%m-%d %H:%M')
    return dt

def read_density_csv(csvfile):
    # Reads into a dataframe object
    df = pd.read_csv(csvfile, index_col=0, sep=', ', parse_dates=['Time'], date_parser=convert_time)

    # Load the csv data
    depths= np.array([float(ii) for ii in df.columns.values])
    rho_obs_tmp = df[:].values.astype(float)
    time = df.index[:]

    # Clip the top
    rho_obs_2d = rho_obs_tmp[:,:]

    # Remove some nan
    fill_value = 1024.
    rho_obs_2d[np.isnan(rho_obs_2d)] = fill_value
    
    return xr.DataArray(rho_obs_2d,dims=('time', 'depth'),
            coords={'time':time.values,'depth':depths})



#######
# Inputs
basedir = '/home/suntans/Share/ScottReef/DATA'
#depthfile = '%s/BATHYMETRY/ETOPO1/ETOPO1_Bed_TimorSea.nc'%basedir
#depthfile = '%s/BATHYMETRY/OTPS_Grid_Ind2016.nc'%basedir
#depthfile = '%s/BATHYMETRY/GEBCO_2014_TimorSea.nc'%basedir

carstempfile = '%s/OCEAN/CARS/temperature_cars2009a.nc'%basedir
carssaltfile = '%s/OCEAN/CARS/salinity_cars2009a.nc'%basedir

rhodatafile = '../DensityFitting/run_ddcurves/DATA_SHELL/Crux_KP150_12mth_Density_lowpass'

## Prelude transect
x0 = 122.840
y0 = -13.080

x1 = 123.519
y1 = -14.002

# Prelude point
xpt = 123.3506
ypt = -13.7641

x0_TS = x0
y0_TS = y0

### Rowley
#x0 = 119.005
#y0 = -17.796
#
#x1 = 120.005
#y1 = -18.938


## NRA Transet
#x0 = 115.829
#y0 = -19.269
#
#x1 = 116.418
#y1 = -20.057
#
#x0_TS = 115.829
#y0_TS = -19.0
#
#xpt = 116.
#ypt = -19.5


dx = 250/1e5 # topo spacing (degrees)
# KdV parameters
dz = 5.0
dxkdv = 50.
h0 = -260
#h0 = -608.8125

#t0 = datetime(2017,4,3)
#t0 = datetime(2017,4,1)
#t0 = datetime(2017,4,18)
t0 = datetime(2016,8,1)
mode =0
spongedist = 1e4

outfile_h = 'data/kdv_bathy_Prelude.csv'
outfile_rho = 'data/kdv_density_Prelude_March.csv'

# Guess and bounds for density fitting
H = -h0
rho0 = 1024


#density_func = double_tanh_exp
#initguess = [rho0, 1.0, H/10, H/5, H/10., H/10., 0.5, 0.5, 1000.] # double tanh exp guess
#bounds = [\
#    (rho0-5,0.,0.,H/20.,0.,H/20.,0.1,0.1,100.),\
#    (rho0+5,10.,H,H,H,H,1,1,5000.)]


density_func = double_tanh_new
initguess = [rho0, 1.0, H/10, H/5, H/10., H/10., 0.5, ] # double tanh exp guess
bounds = [\
    (rho0-5,0.,0.,H/20.,0.,H/20.,0.01,),\
    (rho0+5,10.,H,H,H,H,1,)]

#density_func = 'double_tanh_new'
#bounds=None
#initguess = None

#######

print('Load the station data...')
# Load the csv data
rho = read_density_csv('%s.csv'%rhodatafile)
#nt,nz = rho.shape
#depths_2d = rho.depth.values[np.newaxis,:].repeat(nt, axis=0)

def fit_obs_cars_rho(t0, rhoobs, zobs):
    #print( 'Getting the CARS data...')
    # Get the CARS data
    t0str = t0.strftime('%Y-%m-%d %H:%M:%S')
    zrho = np.arange(h0,0, 30.)

    T = load_cars_temp(carstempfile, np.array([x0_TS]), np.array([y0_TS]),\
         zrho, [t0] )[:,0,0]

    S = load_cars_temp(carssaltfile, np.array([x0_TS]), np.array([y0_TS]),\
         zrho, [t0] )[:,0,0]

    # Fit the density at the boundary

    # Use the wrapper class to compute density on a constant grid
    # 
    # Insert CARS data only to get rho
    iwcars = IWaveModes(T, zrho, salt=S, density_class=InterpDensity,\
        density_func=density_func)

    # Combine the CARS and observed density
    idx = zrho < -500

    zall = np.hstack([zrho[idx], zobs])
    rhoall = np.hstack([iwcars.rho[idx], rhoobs])

    iw = IWaveModes(rhoall, zall,\
        density_class=FitDensity,\
        density_func=density_func,
        bounds=bounds, initguess=initguess)


    phi, c1, he, Z = iw(h0,dz,mode)

    
    
    #print(iw.Fi.f0)
    outstr = t0str
    for ff in iw.Fi.f0:
        outstr += ', %3.6f'%ff
    print(outstr)

    #iw.plot_modes();plt.show()

    return iw.Fi.f0
    #
    #rho_ds = np.array([iw.Z, iw.rhoZ]).T

    #np.savetxt(outfile_rho, rho_ds, delimiter=',', fmt='%3.6f')
    #print('Save files {} and {}'.format(outfile_h, outfile_rho))

#t0str = t0.strftime('%Y-%m-%d')
#rhonow = rho.sel(time=t0str)
#rhoobs = rhonow[0,:].values
#zobs = rho.depth.values

for ii,t in enumerate(rho.time.values):
    rhoobs = rho[ii,:].values
    zobs = rho.depth.values

    t0 = datetime64todatetime([t])[0]

    fit_obs_cars_rho(t0, rhoobs, zobs)

    
