REM print the help
kdv_shelf.exe -h

REM run the model
kdv_shelf -f data/kdvin.yml --rhofile data/kdv_density_NRA_March.csv --depthfile data/kdv_bathy_NRA_flatshelf.csv --a0 15. --outfile kdv_test.nc

pause
