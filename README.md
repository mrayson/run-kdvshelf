# KdV Model standalone executable

## Contents:

 - `kdv_shelf.exe` : kdv model executable
 - `run_kdv.bat`: example windows batch script to run the model
 - `data/kdvin.yml`: example model input parameter file
 - `data/kdv_bathy_NRA_flatshelf.csv`: example bathymetry file
 - `data/kdv_density_NRA_March.csv`: example density profile file

## Usage:

In a command prompt, type:

    `kdv_shelf.exe -h`

for help.

## To compile:

- Install `pyinstaller` package: `pip install pyinstaller`

- In a command prompt:
    
    `pyinstaller --onefile SCRIPTS\kdv_shelf.py`

- Copy the exe from the `dist` folder

---

Matt Rayson

University of Western Australia

November 2018
